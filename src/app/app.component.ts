import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'angular-base-app';
  url = './../assets/micro-fe.js';
  urltwo = './../assets/micro-fe-two.js';
  flag = false;
  constructor() {
    setTimeout(() => {
      this.flag = true;
    }, 5000);
  }
}
